﻿using System;
using System.Threading;

namespace SisBancario
{
    class Conta
    {
        private decimal _saldo = 0;

        public string Tipo { get; set; }

        public void Depositar(decimal quantidade)
        {
            _saldo += quantidade;
        }

        public void Sacar(decimal quantidade)
        {
            _saldo -= quantidade;
        }

        public void ConsultarSaldo()
        {
            Console.WriteLine("Saldo: " + _saldo);
            Thread.Sleep(1000 * 5);
        }
    }

    class Program
    {
        private static Conta Conta { get; set; }

        static void Main(string[] args)
        {
            var criarConta = GerarMenuCriarConta();
            switch (criarConta)
            {
                case 1:
                    Conta = new Conta { Tipo = "Poupança" };
                    break;
                case 2:
                    Conta = new Conta { Tipo = "Corrente" };
                    break;
                default:
                    Environment.Exit(0);
                    break;
            }

            int opcao;
            while ((opcao = GerarMenuConta(Conta.Tipo)) != 5)
            {
                switch (opcao)
                {
                    case 1:
                        Console.WriteLine("Quanto deseja depositar?");
                        Conta.Depositar(decimal.Parse(Console.ReadLine()));
                        break;
                    case 2:
                        Console.WriteLine("Quanto deseja sacar?");
                        Conta.Sacar(decimal.Parse(Console.ReadLine()));
                        break;
                    case 3:
                        Conta.ConsultarSaldo();
                        break;
                }
            }
        }

        static int GerarMenuCriarConta()
        {
            Console.Clear();
            Console.WriteLine("\nEscolha uma opção: \n");
            Console.WriteLine("\n1 - Criar conta poupança\n");
            Console.WriteLine("\n2 - Criar conta corrente\n");
            Console.WriteLine("\n5 - Sair\n");

            return int.TryParse(Console.ReadLine(), out var result) ? result : 5;
        }

        static int GerarMenuConta(string tipo)
        {
            Console.Clear();
            Console.WriteLine("Conta " + tipo);
            Console.WriteLine("\n1 - Depositar\n");
            Console.WriteLine("\n2 - Sacar\n");
            Console.WriteLine("\n3 - Consultar saldo\n");
            Console.WriteLine("\n5 - Sair\n");

            return int.TryParse(Console.ReadLine(), out var result) ? result : 5;
        }
    }
}
